/*
 * WordProps.ts
 * Defines the properties of the Word component
 * Created on 8/21/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import
import { SyntaxColor } from '../edit/SyntaxColor';

/**
 * The properties of a [[Word]] of LMC code
 */
export interface WordProps {
	/**
	 * The `Word`'s text
	 */
	text: string;

	/**
	 * Whether the `Word` should be highlighted
	 */
	highlight?: boolean;

	/**
	 * The syntax coloring of the `Word`
	 */
	color: SyntaxColor;
}

//end of file
