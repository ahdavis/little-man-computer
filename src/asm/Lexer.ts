/*
 * Lexer.ts
 * Defines a class that lexes LMC code
 * Created on 8/22/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { Token } from './Token';
import { Constants } from '../util/Constants';

/**
 * Lexes LMC code and generates tokens
 */
export abstract class Lexer {
	//constants
	/**
	 * Used to lex newlines
	 */
	protected static readonly NEWLINE_RGX: RegExp = /\r\n|\r|\n/g; 

	/**
	 * The character that starts a comment
	 */
	protected static readonly COMMENT_CHAR: string = '#';

	//fields

	/**
	 * The text being lexed
	 */
	protected _text: string;

	/**
	 * The current position in the text
	 */
	protected _pos: number;

	/**
	 * The current character being lexed
	 */
	protected _curChar: string | null;

	/**
	 * The current line number being lexed
	 */
	protected _curLine: number;

	/**
	 * The current column number being lexed
	 */
	protected _curCol: number;

	//methods

	/**
	 * Constructs a new `Lexer` instance
	 *
	 * @param newText The text to lex
	 */
	constructor(newText: string) {
		this._text = newText; //init the text 
		this._pos = 0; //set the position to the beginning
		this._curChar = this._text[this._pos]; //init the character
		this._curLine = 1; //init the line number
		this._curCol = 0; //init the column number
	}

	//getters

	/**
	 * Gets the current line number
	 *
	 * @returns The current line number
	 */
	public get curLine(): number {
		return this._curLine;
	}

	/**
	 * Gets the current column number
	 *
	 * @returns The current column number
	 */
	public get curCol(): number {
		return this._curCol;
	}

	//other methods

	/**
	 * Gets the next `Token` consumed from the text
	 *
	 * @returns The next `Token` consumed from the text
	 */
	public abstract nextToken(): Token;

	/**
	 * Advances the lexer to the next character
	 */
	protected advance(): void {
		this._curCol++; //advance the column
		this._pos++; //advance the position

		//and get the next character
		if(this._pos > (this._text.length - 1)) {
			this._curChar = null;
		} else {
			this._curChar = this._text[this._pos];
		}
	}

	/**
	 * Skips whitespace in the input
	 */
	protected skipWhitespace(): void {
		//save the current column number
		let saveCol = this._curCol;

		//loop through the whitespace
		while((this._curChar !== null) &&
			/^\s*$/.test(this._curChar)) {
			this.advance();
		}

		//and restore the column number
		this._curCol = saveCol;
	}

	/**
	 * Skips newlines in the input
	 */
	protected skipNewlines(): void {
		this.advance(); //advance past the newline
		this._curLine++; //advance the line counter
		this._curCol = 0; //and reset the column counter
	}

	/**
	 * Skips comments in the input
	 *
	 * @returns The skipped comment
	 */
	protected skipComments(): string {
		//declare the return value
		let ret = '';

		//loop to the end of the line
		while((this._curChar !== null) &&
			(!Lexer.NEWLINE_RGX.test(this._curChar))) {
			ret += this._curChar;
			this.advance();
		}

		//advance past the newline
		this.advance();

		//and return the comment string
		return ret;
	}

	/**
	 * Returns an integer consumed from the input
	 *
	 * @returns An integer string consumed from the input
	 */
	protected integer(): string {
		//declare variables
		let regex = new RegExp('[0-9]'); //used to match digits
		let ret = ''; //the return value

		//loop and generate the string
		while((this._curChar !== null) &&
			(regex.test(this._curChar))) {
			ret += this._curChar;
			this.advance();
		}

		//and return the populated string
		return ret;
	}

	/**
	 * Returns an instruction or text consumed from the input
	 *
	 * @returns An instruction or text consumed from the input
	 */
	protected instrOrText(): string {
		//declare variables
		let regex = new RegExp('[a-zA-Z]');
		let ret = ''; //the return value

		//loop and generate the string
		while((this._curChar !== null) &&
			regex.test(this._curChar)) {
			ret += this._curChar;
			this.advance();
		}

		//and return the string
		return ret;
	}

	/**
	 * Returns whether a character is whitespace
	 *
	 * @param ch The character to check
	 *
	 * @returns Whether the character is whitespace
	 */
	protected static charIsWS(ch: string | null): boolean {
		if(ch) {
			if(ch === ' ') {
				return true;
			} else if(ch === '\n') {
				return true;
			} else if(ch === '\t') {
				return true;
			} else if(ch === '\r') {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}

//end of file
