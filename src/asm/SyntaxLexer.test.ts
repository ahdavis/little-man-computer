/*
 * SyntaxLexer.test.ts
 * Defines unit tests for the SyntaxLexer class
 * Created on 8/22/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { Token } from './Token';
import { TokenType } from './TokenType';
import { SyntaxLexer } from './SyntaxLexer';

//define the test text
const text: string = `INP
			STA FIRST
			INP # COMMENT
			STA SECOND 
			LDA FIRST
			SUB SECOND
			OUT
			HLT
			FIRST DAT 
			SECOND DAT
			THIRD DAT 42
			NONE`;

//start of tests
describe('SyntaxLexer', () => {
	//this test checks that column and line breaks are lexed properly
	it('Column and line breaks lexed properly', () => {
		let lexer = new SyntaxLexer(text);
		let token = lexer.nextToken();
		expect(lexer.curLine).toEqual(1);
		expect(lexer.curCol).toEqual(3);
		token = lexer.nextToken();
		expect(lexer.curLine).toEqual(2);
		expect(lexer.curCol).toEqual(3);
		token = lexer.nextToken();
		expect(lexer.curLine).toEqual(2);
		expect(lexer.curCol).toEqual(8);
	});

	//this test checks token generation
	it('Tokens generated properly', () => {
		//create the lexer
		let lexer = new SyntaxLexer(text);

		//create the type array
		const types = [
			TokenType.INSTR,
			TokenType.INSTR,
			TokenType.TXT,
			TokenType.INSTR,
			TokenType.COM,
			TokenType.INSTR,
			TokenType.TXT,
			TokenType.INSTR,
			TokenType.TXT,
			TokenType.INSTR,
			TokenType.TXT,
			TokenType.INSTR,
			TokenType.INSTR,
			TokenType.TXT,
			TokenType.INSTR,
			TokenType.TXT,
			TokenType.INSTR,
			TokenType.TXT,
			TokenType.INSTR,
			TokenType.NUM,
			TokenType.TXT 
		];

		//create the value array
		const values = [
			'INP',
			'STA',
			'FIRST',
			'INP',
			'# COMMENT',
			'STA',
			'SECOND',
			'LDA',
			'FIRST',
			'SUB',
			'SECOND',
			'OUT',
			'HLT',
			'FIRST',
			'DAT',
			'SECOND',
			'DAT',
			'THIRD',
			'DAT',
			'42',
			'NONE'
		];

		//and execute the tests
		let token = lexer.nextToken();
		let i = 0;
		while(token.tokenType !== TokenType.EOF) {
			expect(token.tokenType).toEqual(types[i]);
			expect(token.value).toEqual(values[i]);
			i++;
			token = lexer.nextToken();
		}
		expect(token.tokenType).toEqual(TokenType.EOF);
		token = lexer.nextToken();
		expect(token.tokenType).toEqual(TokenType.EOF);

	});
});

//end of tests
