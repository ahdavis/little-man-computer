/*
 * SyntaxLexer.ts
 * Defines a class that lexes LMC code for syntax highlighting
 * Created on 8/22/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { Lexer } from './Lexer';
import { LexerError } from '../error/LexerError';
import { Token } from './Token';
import { TokenType } from './TokenType';
import { Constants } from '../util/Constants';

/**
 * Lexes LMC code for syntax highlighting
 */
export class SyntaxLexer extends Lexer {
	//no fields

	//methods

	/**
	 * Constructs a new `SyntaxLexer` instance
	 *
	 * @param text The text to lex
	 */
	constructor(text: string) {
		super(text); //call the superclass constructor
	}

	/**
	 * Returns the next `Token` consumed from the input
	 *
	 * @returns The next `Token` consumed from the input
	 */
	public nextToken(): Token {
		//loop and generate tokens
		while(this._curChar !== null) {
			//handle whitespace
			if((this._curChar === ' ') ||
				(this._curChar === '\t')) {
				this.skipWhitespace();
				continue;
			}

			//handle newlines
			if(Lexer.NEWLINE_RGX.test(this._curChar)) {
				this.skipNewlines();
				continue;
			}

			//handle comments
			if(this._curChar === Lexer.COMMENT_CHAR) {
				return new Token(TokenType.COM, 
						this.skipComments());
			}

			//handle integer literals
			if(/[0-9]/.test(this._curChar)) {
				return new Token(TokenType.NUM,
						this.integer());
			}

			//handle text
			if(/[a-zA-Z]/.test(this._curChar)) {
				//lext the text
				let res = this.instrOrText();

				if(Constants.INSTRUCTIONS
					.includes(res)) {
					//instruction
					return new Token(TokenType.INSTR,
							res);
				} else { //text
					return new Token(TokenType.TXT,
								res);
				}
			}

			//if control reaches here,
			//then an unknown character was found
			//so we throw an exception
			throw new LexerError(this._curChar, this._curLine,
						this._curCol);
		}

		//finally, return an EOF token
		return Token.EOFToken;
	}
}

//end of file
