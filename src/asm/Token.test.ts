/*
 * Token.test.ts
 * Defines unit tests for the Token class
 * Created on 8/22/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { Token } from './Token';
import { TokenType } from './TokenType';

//start of tests
describe('Token', () => {
	//this test ensures the validity of an EOF token
	it('EOF token is valid', () => {
		let tok = Token.EOFToken;
		expect(tok.tokenType).toEqual(TokenType.EOF);
		expect(tok.value).toEqual('');
	});
});

//end of tests
