/*
 * Token.ts
 * Defines a class that represents a lexer token
 * Created on 8/22/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import
import { TokenType } from './TokenType';

/**
 * A lexer token
 */
export class Token {
	//constant
	/**
	 * An EOF `Token`
	 */
	public static readonly EOFToken: Token 
				= new Token(TokenType.EOF, '');

	//fields

	/**
	 * The type of the `Token`
	 */
	private _tokenType: TokenType;

	/**
	 * The value of the `Token`
	 */
	private _value: string;

	//methods

	/**
	 * Constructs a new `Token` instance
	 *
	 * @param newType The type of the `Token`
	 * @param newValue The value of the `Token`
	 */
	constructor(newType: TokenType, newValue: string) {
		this._tokenType = newType;
		this._value = newValue;
	}

	//getters

	/**
	 * Gets the type of the `Token`
	 *
	 * @returns The type of the `Token`
	 */
	public get tokenType(): TokenType {
		return this._tokenType;
	}

	/**
	 * Gets the value of the `Token`
	 *
	 * @returns The value of the `Token`
	 */
	public get value(): string {
		return this._value;
	}
}

//end of file
