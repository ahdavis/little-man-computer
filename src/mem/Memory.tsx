/*
 * Memory.tsx
 * Defines a React component that represents memory
 * Created on 8/20/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import * as React from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import { MemCell } from './MemCell';
import { MemoryProps } from '../props/MemoryProps';
import { MemoryState } from '../state/MemoryState';
import { Constants } from '../util/Constants';

/**
 * The Little Man Computer's memory
 */
export class Memory extends React.Component<MemoryProps, MemoryState> {
	//field
	/**
	 * The [[MemCell]]s that contain data
	 */
	private _cells: React.RefObject<MemCell>[];

	//methods
	
	/**
	 * Constructs a new `Memory` instance
	 *
	 * @param props The properties of the component
	 */
	constructor(props: MemoryProps) {
		super(props); //call the superclass constructor

		//init the cell array
		let sz = this.props.size;
		this._cells = new Array<React.RefObject<MemCell>>(sz);
		for(let i = 0; i <= sz; i++) {
			this._cells[i] = React.createRef<MemCell>();
		}

		//create the value array
		let valArr = new Array<number>(this.props.size);

		//and add it to the state
		this.state = {
			values: valArr 
		};
	}

	/**
	 * Returns the value of the memory cell
	 * at a given address. Returns `null`
	 * if the address is out of range, or if
	 * the memory cell does not exist
	 *
	 * @param addr The address to get the state for
	 *
	 * @returns The value of the `MemCell` at the given address
	 */
	public getValueAtAddress(addr: number): number | null {
		//validate the address
		if((addr < 0) || (addr > this.props.size)) {
			return null;
		}

		//get the cell reference at the address
		const ref = this._cells[addr].current;

		//and return its value
		if(ref) {
			return ref.intValue;
		} else {
			return null;
		}
	}

	/**
	 * Sets the value at a given memory address
	 *
	 * @param addr The address to modify
	 * @param value The new value of that address
	 *
	 * @returns Whether the address was modified successfully
	 */
	public setValueAtAddress(addr: number, value: number): boolean {
		//validate the address
		if((addr < 0) || (addr > this.props.size)) {
			return false;
		}

		//and update the value at the address
		if(this._cells[addr].current) {
			this._cells[addr].current!.intValue = value;
			this.refreshState();
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Renders the component
	 *
	 * @returns A React DOM representing the component
	 */
	public render(): React.ReactNode {
		//get the height of the scrollbar
		let height = Constants.WIN_HEIGHT - 25;

		//and create the DOM and return it
		return (
			<Scrollbars style={{width: 100}}
					autoHeight
					autoHeightMin={height}
					autoHeightMax={height} >
				{this.generateCells()}
			</Scrollbars>
		);
	}

	/**
	 * Called when the component renders
	 */
	public componentDidMount(): void {
		//refresh the state of the component
		this.refreshState();
	}


	/**
	 * Generates the [[MemCell]]s that make up memory
	 *
	 * @returns An array of `MemCell` components
	 */
	private generateCells(): React.ReactNode[] {
		//declare the return value
		let ret: React.ReactNode[] = [];

		//loop and generate the array
		for(let i = 0; i <= this.props.size; i++) {
			ret.push(<MemCell address={i} 
					ref={this._cells[i]}
					key={i} />);
		}

		//and return the generated array
		return ret;
	}

	/**
	 * Refreshes the state of the component
	 */
	private refreshState(): void {
		//declare the state buffer
		let buffer = new Array<number>(this.props.size);
		for(let i = 0; i <= this.props.size; i++) {
			buffer[i] = 0;
		}

		//loop and update the state buffer
		for(let i = 0; i <= this.props.size; i++) {
			//get the current cell
			const ref = this._cells[i].current;

			//update the buffer
			if(ref) {
				buffer[i] = ref.intValue;
			}
		}

		//and update the component's state
		this.setState({
			values: buffer
		});
	}
}

//end of file
