/*
 * Memory.test.tsx
 * Defines unit tests for the Memory component
 * Created on 8/20/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import * as React from 'react';
import { shallow, mount } from 'enzyme';
import { Memory } from './Memory';

//start of tests
describe('Memory', () => {
	//this test checks that the memory matches its snapshot
	it('Renders to snapshot', () => {
		const mem = shallow(<Memory size={99} />)
		expect(mem).toMatchSnapshot();
	});

	//this test checks that values of memory cells reflect
	//in the "values" state field
	it('State reflects children', () => {
		const component = mount<Memory>(<Memory size={99} />);
		expect(component.state().values[99]).toEqual(0); 
	});

	//this test checks that memory can be modified
	it('Cells can be modified', () => {
		let component = mount<Memory>(<Memory size={99} />);
		expect(component.instance().getValueAtAddress(3))
			.toBe(0);
		let res = component.instance().setValueAtAddress(3, 45);
		expect(res).toBeTruthy();
		expect(component.instance().getValueAtAddress(3))
			.toBe(45);

		expect(component.state().values[3]).toBe(45);
	});

	//this test checks that out of range memory can't be accessed
	it('Memory range validation works', () => {
		let component = mount<Memory>(<Memory size={99} />);
		expect(component.instance().getValueAtAddress(-1))
			.toBeNull();
		expect(component.instance().getValueAtAddress(100))
			.toBeNull();
		expect(component.instance().setValueAtAddress(-1, 34))
			.toBeFalsy();
		expect(component.instance().setValueAtAddress(100, 34))
			.toBeFalsy();
	});

});

//end of tests
