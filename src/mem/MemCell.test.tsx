/*
 * MemCell.test.tsx
 * Defines unit tests for the MemCell component
 * Created on 8/19/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import * as React from 'react';
import { shallow } from 'enzyme';
import { MemCell } from './MemCell';

//start of tests
describe('MemCell', () => {
	//this test checks that the cell matches its snapshot
	it('Renders to snapshot', () => {
		const cell = shallow(<MemCell address={1} />)
		expect(cell).toMatchSnapshot();
	});

	//this test checks that input events are handled properly
	it('Handles input', () => {
		const inValue = '245';
		const event = {
			preventDefault() {},
			target: { value: inValue }
		};
		const component = shallow(<MemCell address={1} />)
		component.find('input').simulate('change', event);
		expect(component.state('value')).toEqual(inValue);
	});

	//this test checks that values with less than 3 digits
	//are padded with zeroes
	it('Values are padded with zeroes', () => {
		let testValues = ['1', '42', '256'];
		let padValues = ['001', '042', '256'];
		for(let i = 0; i < testValues.length; i++) {
			let test = testValues[i];
			let pad = padValues[i];
			const event = {
				preventDefault() {},
				target: { value: test }
			};
			const component = shallow(<MemCell address={1} />);
			component.find('input').simulate('change', event);
			expect(component.state('value')).toEqual(pad);
		}
	});

	//this test checks that addresses are two digits
	it('Addresses are two digits', () => {
		let testValues = [0, 10];
		let padValues = ['00', '10'];
		for(let i = 0; i < testValues.length; i++) {
			let test = testValues[i];
			let pad = padValues[i];
			const component = shallow(<MemCell 
							address={test} />);
			expect(component.state('address')).toEqual(pad);
		}
	});
});

//end of tests
