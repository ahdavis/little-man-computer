/*
 * MemCell.tsx
 * Defines a component that represents a memory cell
 * Created on 8/5/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import * as React from 'react';
import { MemCellProps } from '../props/MemCellProps';
import { MemCellState } from '../state/MemCellState';

/**
 * A memory cell in the computer
 */
export class MemCell extends React.Component<MemCellProps, MemCellState> {
	//no fields

	/**
	 * Constructs a new `MemCell` component
	 *
	 * @param props The properties of the cell
	 */
	constructor(props: MemCellProps) {
		super(props); //call the superclass constructor

		//and initialize the state field
		this.state = {
			value: "000", //the initial value is 0
			address: this.padAddress() //init the address
		};
	}

	/**
	 * Gets the current value of the memory cell
	 *
	 * @returns The current value of the cell as an integer
	 */
	public get intValue(): number {
		return parseInt(this.state.value);
	}

	/**
	 * Sets the current value of the memory cell
	 *
	 * The new value must be between 0 and 999 inclusive.
	 * If it is outside that range, then the value will
	 * be set to 0 or 999, whichever is closer.
	 *
	 * @param newValue The new value of the cell
	 */
	public set intValue(newValue: number) {
		//validate the value
		if(newValue < 0) {
			this.strValue = "000"
		} else if(newValue > 999) {
			this.strValue = newValue.toString().substr(0, 3);
		} else {
			//pad the value with zeroes if
			//it is less than 100
			if((newValue < 100) && (newValue >= 0)){
				let value = newValue.toString();
				for(let i = 0; i < 4 - value.length; i++) {
					value = "0" + value;
				}
				this.strValue = value;
			} else {
				//update the value
				this.strValue = newValue.toString();
			}
		}
	}

	/**
	 * Renders the component
	 *
	 * @returns A React DOM representing the component
	 */
	public render(): React.ReactNode {
		return (
			<div style={this.generateCSS()}>
				<p>Address: {this.state.address}</p>
				<label>
					Value:&nbsp;
					<input type="text"
						pattern="[0-9]*"
						maxLength={4}
						size={2}
				onChange={this.handleChange.bind(this)}
					value={this.state.value}/>
				</label>
			</div>
		);
	}

	/**
	 * Sets the string value of the cell
	 *
	 * @param newValue The new value of the cell
	 */
	private set strValue(newValue: string) {
		this.setState({value: newValue});
	}

	/**
	 * Event handler for changing the value of the cell
	 *
	 * @param event The change event
	 */
	private handleChange(event: 
		React.SyntheticEvent<HTMLInputElement>): void {

		//prevent default behavior
		event.preventDefault();
		
		//get the target
		let target = event.target as HTMLInputElement;

		//get its values
		let strV = target.value;
		let value = parseInt(target.value);

		//check for backspace
		if(isNaN(value) && strV.length <= 1) {
			this.strValue = "";
			return;
		}

		//and update the value
		if(!isNaN(value)) {
			this.intValue = parseInt(target.value);
		}
	}

	/**
	 * Generates the CSS for the `MemCell`
	 *
	 * @returns The CSS for the cell
	 */
	private generateCSS(): React.CSSProperties {
		return {
			border: '2px solid black',
			display: 'inline-block',
			width: 'auto'
		};
	}

	/**
	 * Pads the address property with a leading zero
	 *
	 * @returns The address value with a leading zero
	 */
	private padAddress(): string {
		//get the address string
		let addrStr = this.props.address.toString(); 

		//pad it with zeroes
		if(addrStr.length < 2) {
			addrStr = '0' + addrStr;
		}

		//and return it
		return addrStr;
	}
}

//end of file
