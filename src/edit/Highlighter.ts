/*
 * Highlighter.ts
 * Defines a class that generates syntax highlighting for LMC code
 * Created on 8/21/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { Constants } from '../util/Constants';
import { SyntaxColor } from './SyntaxColor';
import { SyntaxLexer } from '../asm/SyntaxLexer';
import { TokenType } from '../asm/TokenType';
import { Token } from '../asm/Token';

/**
 * Generates syntax highlighting
 */
export class Highlighter {
	//fields
	/**
	 * Used to generate syntax tokens
	 */
	private _lexer: SyntaxLexer;

	/**
	 * The token representing the current symbol
	 * being highlighted
	 */
	private _curToken: Token;

	//methods

	/**
	 * Constructs a new `Highlighter` instance
	 *
	 * @param code The code to generate syntax highlighting for
	 */
	constructor(code: string) {
		//create the lexer
		this._lexer = new SyntaxLexer(code);

		//and create the token
		this._curToken = this._lexer.nextToken();
	}

	/**
	 * Returns the syntax highlight for the next symbol in the code
	 * Returns `null` if at the end of the code
	 *
	 * @returns The proper syntax highlight for that symbol
	 */
	public nextColor(): SyntaxColor | null {
		//loop until the EOF token is generated
		while(this._curToken.tokenType !== TokenType.EOF) {
			//save the current token and get the next one
			let token = this._curToken;
			this._curToken = this._lexer.nextToken();

			//check for a comment
			if(token.tokenType === TokenType.COM) {
				return SyntaxColor.YELLOW;
			}

			//check for a number token
			if(this.tokenIsNum(token)) {
				return SyntaxColor.BLUE;
			} 

			//check for a label token
			if(this.tokenIsLabel(token)) {
				return SyntaxColor.GREEN; 
			}

			//check for an instruction token
			if(this.tokenIsInstr(token)) {
				return SyntaxColor.RED;
			} 
		
			//if control reaches here, then no match
			//was found for the current token
			//so we return NONE, which does not generate
			//any syntax coloring
			return SyntaxColor.NONE;
		}

		//when control reaches here, then the end of
		//the input has been reached, so we return null
		return null;
	}

	/**
	 * Returns whether a token is a numeric literal
	 *
	 * @param tok The token to check
	 *
	 * @returns Whether the argument token is a numeric literal
	 */
	private tokenIsNum(tok: Token): boolean {
		if(tok.tokenType === TokenType.NUM) {
			let num = parseInt(tok.value);
			if((num >= 0) && (num <= 999)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * Returns whether a token is an instruction
	 *
	 * @param tok The token to check
	 *
	 * @returns Whether the argument token is an instruction
	 */
	private tokenIsInstr(tok: Token): boolean {
		return tok.tokenType === TokenType.INSTR;
	}

	/**
	 * Returns whether a token is a label
	 *
	 * @param tok The token to check
	 *
	 * @returns Whether the argument token is a label
	 */
	private tokenIsLabel(tok: Token): boolean {
		//ensure that the current token is a text token
		if(tok.tokenType !== TokenType.TXT) {
			return false;
		}

		//and check to see if text or an instruction follows
		//the first token
		if(this._curToken.tokenType === TokenType.INSTR) {
			return true;
		} else if(this._curToken.tokenType === TokenType.TXT) {
			return true;
		} else {
			return false;
		}
	}
}

//end of file
