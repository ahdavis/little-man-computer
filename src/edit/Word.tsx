/*
 * Word.tsx
 * Defines a React component that represents a single word of LMC code
 * Created on 8/21/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import * as React from 'react';
import { WordProps } from '../props/WordProps';
import { WordState } from '../state/WordState';
import { SyntaxColor } from './SyntaxColor';

/**
 * A single word of LMC code
 */
export class Word extends React.Component<WordProps, WordState> {
	/**
	 * Constructs a new `Word` component
	 *
	 * @param props The properties of the component
	 */
	constructor(props: WordProps) {
		super(props); //call the superclass constructor

		//init the state
		let text = this.props.text;
		if(this.props.color !== SyntaxColor.YELLOW) {
			text = text.split(' ')[0];
		}
		this.state = {
			trimmedText: text 
		};
	}

	/**
	 * Renders the component
	 *
	 * @returns A React DOM that represents the component
	 */
	public render(): React.ReactNode {
		return (
			<span style={this.composeCSS()}>
				{this.state.trimmedText}
			</span>
		);
	}

	/**
	 * Composes the CSS for the component
	 *
	 * @returns The CSS for the component
	 */
	private composeCSS(): React.CSSProperties {
		//determine the color string
		let colorStr = 'pink';
		switch(this.props.color) {
			case SyntaxColor.BLUE: {
				colorStr = 'blue';
				break;
			}
			case SyntaxColor.GREEN: {
				colorStr = 'green';
				break;
			}
			case SyntaxColor.NONE: {
				colorStr = 'black';
				break;
			}
			case SyntaxColor.RED: {
				colorStr = 'red';
				break;
			}
			case SyntaxColor.YELLOW: {
				colorStr = 'yellow';
				break;
			}
			default: {
				break;
			}
		}

		//determine whether to highlight the word
		let bColor = '#FFFFFF00';
		if(this.props.highlight && 
			(this.props.color !== SyntaxColor.YELLOW)) {
			bColor = '#FFFF33';
		}

		//and return the CSS
		return {
			color: colorStr,
			backgroundColor: bColor
		};
	}
}

//end of file
