/*
 * Highlighter.test.ts
 * Defines unit tests for the Highlighter class
 * Created on 8/21/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { Highlighter } from './Highlighter';
import { SyntaxColor } from './SyntaxColor';

//create test code
const code = `
	INP
	STA FIRST
	OUT # COMMENT
	FIRST DAT 42
	LABEL LABEL DAT
	NOLABEL 42 1000
	NOLABEL 
`;

//start of tests
describe('Highlighter', () => {
	//this test checks syntax color generation
	it('Syntax color generation works', () => {
		let hl = new Highlighter(code);
		expect(hl.nextColor()).toEqual(SyntaxColor.RED);
		expect(hl.nextColor()).toEqual(SyntaxColor.RED);
		expect(hl.nextColor()).toEqual(SyntaxColor.GREEN);
		expect(hl.nextColor()).toEqual(SyntaxColor.RED);
		expect(hl.nextColor()).toEqual(SyntaxColor.YELLOW);
		expect(hl.nextColor()).toEqual(SyntaxColor.GREEN);
		expect(hl.nextColor()).toEqual(SyntaxColor.RED);
		expect(hl.nextColor()).toEqual(SyntaxColor.BLUE);
		expect(hl.nextColor()).toEqual(SyntaxColor.GREEN);
		expect(hl.nextColor()).toEqual(SyntaxColor.GREEN);
		expect(hl.nextColor()).toEqual(SyntaxColor.RED);
		expect(hl.nextColor()).toEqual(SyntaxColor.NONE);
		expect(hl.nextColor()).toEqual(SyntaxColor.BLUE);
		expect(hl.nextColor()).toEqual(SyntaxColor.NONE);
		expect(hl.nextColor()).toEqual(SyntaxColor.NONE);
		expect(hl.nextColor()).toBeNull();
		expect(hl.nextColor()).toBeNull();
	});
});

//end of test
