/*
 * Word.test.tsx
 * Defines unit tests for the Word component
 * Created on 8/21/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import * as React from 'react';
import { shallow, mount } from 'enzyme';
import { Word } from './Word';
import { SyntaxColor } from './SyntaxColor';

//start of tests
describe('Word', () => {
	//this test checks that the word matches its snapshot
	it('Renders to snapshot', () => {
		const word = shallow(<Word text="ADD" 
					color={SyntaxColor.RED}/>);
		expect(word).toMatchSnapshot();
	});

	//this test checks that text is trimmed on spaces
	it('Text is trimmed on spaces', () => {
		const word = shallow<Word>(<Word text="ADD STA SUB" 
					color={SyntaxColor.RED}/>);
		expect(word.state().trimmedText).toEqual('ADD');
	});

	//this test checks that comments are not trimmed on spaces
	it('Comments not trimmed on spaces', () => {
		const word = shallow<Word>(<Word text="# COMMENT TEST"
					color={SyntaxColor.YELLOW} />);
		expect(word.state().trimmedText).toEqual('# COMMENT TEST');
	});

	//this test checks that the highlight prop is set correctly
	it('Highlight set correctly', () => {
		const wNoHighlight = mount<Word>(<Word text="Hello" 
					color={SyntaxColor.NONE}/>);
		expect(wNoHighlight.props().highlight).toEqual(undefined);
		const wHighlight = mount<Word>(<Word text="Hello"
						highlight={true} 
					color={SyntaxColor.NONE}/>);
		expect(wHighlight.props().highlight).toEqual(true);
	});

	//this test ensures that comments can't be highlighted
	it('No highlighted comments', () => {
		const comWord = mount<Word>(<Word text="#comment"
					color={SyntaxColor.YELLOW}
					highlight={true} />);
		expect(comWord.find('span').get(0).props.style)
			.toHaveProperty('backgroundColor', '#FFFFFF00');

	});
});

//end of tests
