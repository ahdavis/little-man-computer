/*
 * EditorLexer.ts
 * Defines a class that lexes editable text
 * Created on 8/26/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { Lexer } from '../asm/Lexer';
import { Token } from '../asm/Token';
import { TokenType } from '../asm/TokenType';
import { LexerError } from '../error/LexerError';
import { Constants } from '../util/Constants';

/**
 * Lexes text in an editor interface
 */
export class EditorLexer extends Lexer {
	//no fields

	//methods

	/**
	 * Constructs a new `EditorLexer` instance
	 *
	 * @param text The text to lex
	 */
	constructor(text: string) {
		super(text); //call the superclass constructor
	}

	/**
	 * Generates tokens from the input
	 *
	 * @returns The next `Token` consumed from the input
	 */
	public nextToken(): Token {
		//loop and generate tokens
		while(this._curChar !== null) {
			//handle whitespace
			if((this._curChar === ' ') ||
				(this._curChar === '\t')) {
				this.skipWhitespace();
				continue;
			}

			//handle newlines
			if(Lexer.NEWLINE_RGX.test(this._curChar)) {
				this.skipNewlines();
				continue;
			}

			//handle comments
			if(this._curChar === Lexer.COMMENT_CHAR) {
				return new Token(TokenType.COM,
						this.skipComments());
			}

			//handle integer literals
			if(/[0-9]/.test(this._curChar)) {
				return new Token(TokenType.NUM,
							this.integer());
			}

			//handle text
			if(/[a-zA-Z]/.test(this._curChar)) {
				//consume the text
				let res = this.instrOrText();

				//and determine what it represents
				if(Constants.INSTRUCTIONS.includes(res)) {
					return new Token(TokenType.INSTR,
								res);
				} else {
					return new Token(TokenType.TXT,
								res);
				}
			}

			//if control reaches here, then
			//an unknown character has been found
			//so we throw an error
			throw new LexerError(this._curChar,
						this._curLine,
						this._curCol);
		}
		
		//if control reaches here, then
		//the end of the input was reached
		//so we return an EOF token
		return Token.EOFToken;
	}
}

//end of file
