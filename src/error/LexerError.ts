/*
 * LexerError.ts
 * Defines an error that is thrown when a lexer encounters an error
 * Created on 8/22/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

/**
 * Thrown when a [[Lexer]] encounters an error
 */
export class LexerError extends Error {
	/**
	 * Constructs a new `LexerError` instance
	 *
	 * @param badChar The character that triggered the error
	 * @param line The line number of the bad character
	 * @param col The column number of the bad character
	 */
	constructor(badChar: string, line: number, col: number) {
		//call the superclass constructor
		super('(' + line.toString() + ':' + col.toString() 
			+ ') Unknown character ' + badChar);
	}
}	

//end of file
