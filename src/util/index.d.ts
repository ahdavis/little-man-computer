/*
 * Constants.d.ts
 * TypeScript definitions file for the Constants class
 * Created on 8/21/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

//module declaration
declare module 'Constants' {
	/**
	 * Manages constants for the app
	 */
	class Constants {
		/**
		 * The width of the window
		 */
		static readonly WIN_WIDTH: number;

		/**
		 * The height of the window
		 */
		static readonly WIN_HEIGHT: number;

		/**
		 * The instructions understood by the computer
		 */
		static readonly INSTRUCTIONS: string[];
	}

	//export the class
	export = Constants;
}

//end of file
