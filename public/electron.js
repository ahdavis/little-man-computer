/*
 * electron.js
 * Electron entry point for the Little Man Computer
 * Created on 8/4/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
const electron = require('electron')
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const path = require('path');
const isDev = require('electron-is-dev')
const { Constants } = require('../src/util/Constants.js')

//declare the main window object
let mainWindow;

//this function creates the browser window
function createWindow() {
	//create the window object
	mainWindow = new BrowserWindow({width: Constants.WIN_WIDTH, 
					height: Constants.WIN_HEIGHT});

	//load the HTML interface page
	mainWindow.loadURL(
		isDev ? 'http://localhost:3000'
		: `file://${path.join(__dirname, "../build/index.html")}`
	);

	//and add the closed event to the window
	mainWindow.on('closed', () => (mainWindow = null));
}

//make the app create the window when ready
app.on('ready', createWindow);

//make the app quit when closed ONLY when not on Mac
app.on('window-all-closed', () => {
	if(process.platform !== 'darwin') {
		app.quit();
	}
});

//make the app create the window when activated
app.on('activate', () => {
	if(mainWindow === null) {
		createWindow();
	}
});

//end of file
